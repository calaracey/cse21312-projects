/**********************************************
* File: BTreeInClass.cpp
* Author: [MUST PUT YOUR NAME HERE]
* Email: [MUST PUT YOUR E-MAIL HERE]
*
* This is the driver function for your in-class
* assignment
*
* Must push the following files to GitLab
* BTreeInClass.cpp
* BTree.h - modified with your
* BTreeNode.h - modified with your
**********************************************/
#include <iostream>
#include "BTree.h"
#include <time.h>
#include <cstdlib>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* This is the main driver function for the program
********************************************/
struct Data {
	int key;
	int data;

	Data& operator=(const Data& a) 
	{
		key = a.key;
	}

	bool operator==(const Data& a) const
	{
		return(key == a.key);
	}
	bool operator>(const Data& a) const
	{
		return(key > a.key);
	}	
	bool operator<(const Data& a) const
	{
		return(key < a.key);
	}
};
	std::ostream& operator << (std::ostream& o, const Data& a)
	{
		return o << "key: " << a.key << " value: " << a.data << std::endl;
	}	
	
	
int main(int argc, char** argv){

	// Initial test code
	BTree<struct Data> database(3);
	
	srand(time(NULL));
	
	int i = 0;

	while (i <= 1000) {
		int x = rand() % 100;
		int y = rand() % 100;
		Data a;
		a.key = x;
		a.data = y;
		database.insert(a);

		i++;
	}

	for (int j; j < 11; j++){
		int x = rand() % 100;
		Data a;
	        a.key = x;
		//some problems still: only prints memory addresses. "keys" in B tree are really just structs that also contain
		//values, this seems wrong because there can be duplicate keys
		std::cout << database.search(a) << std::endl;	
	}


	return 0;
}
