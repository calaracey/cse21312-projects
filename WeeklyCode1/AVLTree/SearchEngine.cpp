/**********************************************
* File: SearchEngine.cpp
* Author: Connor Laracey 
* Email: claracey@nd.edu
* Primitive Search Engine using AVL Tree
**********************************************/
//to compile, just do reguler g++ -std=c++11 SearchEngine.cpp -o SearchEngine
#include <iostream>
#include <string>
#include "AVLTree.h"

struct website {
	std::string name;
	std::string url;
	
	website(std::string name, std::string url) : name(name), url(url) {}

	bool operator==(const website& a)const{
		return(url==a.url);
	}
	bool operator>(const website& a)const{
		return(url>a.url);
	}
	bool operator<(const website& a)const{
		return(url<a.url);
	}
};

std::ostream& operator<<(std::ostream& o, const website& outwebsite) {
	o << "website name: " << outwebsite.name << " website url: " << outwebsite.url;

} 

int main(int argc, char **argv) {
	AVLTree<website> AVLengine;
	website website1 ("Google", "https://www.google.com");
	website website2 ("Facebook", "https://www.facebook.com");
	website website3 ("Youtube", "https://www.youtube.com");
	website website4 ("Apple", "https://www.apple.com");
	website website5 ("Amazon", "https://www.amazon.com");
	website website6 ("Microsoft", "https://www.microsoft.com");
	website website7 ("Wikipedia", "https://www.wikipedia.org");
	website website8 ("Gitlab", "https://www.gitlab.com");
	website website9 ("New York Times", "https://www.nyt.com");
	website website10 ("Notre Dame", "https://www.nd.edu");
	//insert websites to avl tree	
	AVLengine.insert(website1);
	AVLengine.insert(website2);
	AVLengine.insert(website3);
	AVLengine.insert(website4);
	AVLengine.insert(website5);
	AVLengine.insert(website6);
	AVLengine.insert(website7);
	AVLengine.insert(website8);
	AVLengine.insert(website9);
	AVLengine.insert(website10);
	//print filled tree
	std::cout << "Printing Tree: \n";
	AVLengine.printTree();
	//remove root
	AVLengine.remove(website3);
	//print new tree
	std::cout << "Printing new Tree, root removed: \n";
	AVLengine.printTree();
}
