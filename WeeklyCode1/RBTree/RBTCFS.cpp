/**********************************************
* File: RBTCFS.cpp
* Author: Connor Laracey
* Email: claracey@nd.edu
* Completely Fair Scheduler using RBTree
**********************************************/
//to compile, do g++ -std=c++11 RBTCFS.cpp -o RBTCFS
#include "RBTree.h"

int totalProcesses = 10;
int longestLen = 1000;

struct Process{
    
    int procNum;
    int vruntime; 
    Process(int procNum, int vruntime) : procNum(procNum), vruntime(vruntime) {}
    
    bool operator<(const Process& a) const{
        return vruntime < a.vruntime;
    }
    bool operator==(const Process& a) const{
        return vruntime == a.vruntime;
    }
    bool operator!=(const Process& a) const{
        return vruntime != a.vruntime;
    }
    friend std::ostream& operator<<(std::ostream& o, const Process& output);
};

std::ostream& operator<<(std::ostream& o, const Process& output){
    o << printProcess.procNum << ", " << printProcess.vruntim << endl;
    
    return o;
}

int main(int argc, char** argv) {
	RBTree<Process> scheduler;
	Process p1(1, 19);
	scheduler.insert(p1);
	Process p2(2,33);
	scheduler.insert(p2);
	Process p3(3,49);
	scheduler.insert(p3);
	Process p4(4, 110);
	scheduler.insert(p4);
	Process p5(5, 32);
	scheduler.insert(p5);
	Process p6(6, 99);
	scheduler.insert(p6);
	Process p7 (7,10);
	scheduler.insert(p7);
	Process p8 (8,20);
	scheduler.insert(p8);
	Process p9 (9,43);
	scheduler.insert(p9);
	Process p10 (10, 133);
	scheduler.insert(p10);


	RBTNode<Process> *getleftnode = scheduler.getRoot();

	while(getleftnode->left != NULL)
		getleftnode = getleftnode->left;
	std::cout << "Left node is " << getleftnode->val << std::endl;
	
	scheduler.deleteByVal(getleftnode->val);

	std::cout << "Deleted left node tree: " << std::endl;

	scheduler.printInOrder();
} 
