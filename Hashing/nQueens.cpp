/**********************************************
* File: nQueens.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* N-Queens solver 
**********************************************/
#include<iostream>
#include<bitset>
#include<sstream>

std::bitset<30> col, d1, d2;

void printBitSets(int nSize){
    
    std::cout << "col: ";
    for(int i = 0; i < nSize; i++){
        std::cout << col[i] << " ";
    }
    std::cout << std::endl;
    
    std::cout << "d1: ";
    for(int i = 0; i < nSize; i++){
        std::cout << d1[i] << " ";
    }
    std::cout << std::endl;
    
    std::cout << "d2: ";
    for(int i = 0; i < nSize; i++){
        std::cout << d2[i] << " ";
    }
    std::cout << std::endl;
}

/***********************************
 * Function Name: nQueensSolve
 * Pre-Conditions: r, nSize, nConfigs
 * Post-Conditions: void
 * 
 * Performs a recursive backtracking call to 
 * solve the N-Queens problem 
 * *********************************/
void nQueensSolve(int r, int nSize, int &nConfigs){
    
    if(r == nSize){ 
        nConfigs++; 
        return;
    }

    for(int c=0; c < nSize; c++){
        if( !col[c] && !d1[r-c+nSize-1] && !d2[r+c]){
            col[c] = d1[r-c+nSize-1] = d2[r+c] = 1;
            nQueensSolve(r+1, nSize, nConfigs);
            col[c] = d1[r-c+nSize-1] = d2[r+c] = 0;
        }
    }
}

/***********************************
 * Function Name: main 
 * Pre-Conditions: int, char**
 * Post-Conditions: void
 * 
 * The main driver function of the program 
 * *********************************/
int main(int argc, char**argv){
    
	std::stringstream intInput(argv[1]);
	
	// Initialize the size
    int nSize;
	
	if(!(intInput >> nSize)){
		std::cout << argv[1] << " is not a valid integer" << std::endl;
		exit(-1);
	}
    
    // Set the number of configurations 
    int nConfigs = 0;
    
    // Call the recursive algorithm
    nQueensSolve(0,nSize,nConfigs);
    
    // Print the output to the user
    std::cout << "The number of configurations for a " << nSize << "-Queen Problem is "; 
    std::cout << nConfigs << std::endl;

    return 0;
}
