/*****************************************
 * Filename: Section01_pointers2.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file demonstrates the purpose of * and &
 * and shows the results of several pointer 
 * operations, using new int.
 * **************************************/

#include<iostream>

/************************************
 * Function name: main
 * Preconditions: int, char **
 * Postconditions: int 
 * 
 * This is the main driver function
 * *********************************/
int main(int argc, char**argv)
{

	int *p1, *p2; // Integer pointers 
	
	// Allocate a register for p1 and p2 as an integer
	p1 = new int;
	p2 = new int;
	
	*p1 = -100; *p2 = 200;
	
	// Output the value stores in p1 and p2, which contain
	// the addresses of the registers that contain the new ints 
	std::cout << p1 << " " << p2 << std::endl;
	
	// Output the addresses of p1 and p1 registers
	std::cout << &p1 << " " << &p2 << std::endl;
	
	// Outputs the value contained at the address of p1 and p2
	// Therefore, will output the values -100 and 200
	std::cout << *p1 << " " << *p2 << std::endl << std::endl;
	
	// Sets the value in the address pointed to be p1 equal to 300
	// Therefore, this sets the new integer p1 is pointing to equal to 300
	*p1 = 300;
	
	// Sets the address pointed to by p1 equal to the address pointed
	// to be p2. Therefore, the registers for p1 and p2 contain the 
	// same address.
	p1 = p2;
	
	// Output the value stores in p1 and p2, which contain
	// the addresses of the registers. Both point to y 
	std::cout << p1 << " " << p2 << std::endl;
	
	// Output the addresses of p1 and p1 registers. Remain unchanged
	std::cout << &p1 << " " << &p2 << std::endl;
	
	// Outputs the value contained at the address of p1 and p2
	// Therefore, will output the values 200 and 200	
	std::cout << *p1 << " " << *p2 << std::endl << std::endl;

	// Sets the value in the address pointed to be p1 equal to 300
	// Therefore, this sets y equal to -100	
	*p1 = -100;
	
	// Output the value stores in p1 and p2, which contain
	// the addresses of the registers. Both point to y 
	std::cout << p1 << " " << p2 << std::endl;
	
	// Outputs the value contained at the address of p2
	// Therefore, will output the values of y and x, -100 and -100
	std::cout << *p1 << " " << *p2 << std::endl << std::endl;
	
	// Since p1 is a new int, delete p1 since we allocated that memory
	delete p1; // no need for delete p2, since they were pointing to the same register

	return 0;
}