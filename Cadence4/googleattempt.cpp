/**********************************************
 * File: google.cpp
 * Author: Connor Laracey
 * Email: claracey@nd.edu
 * attempts to find the shortest word in license plate
 **********************************************/


#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <set>


struct dictstruct {
	dictstruct() {
		std::ifstream input("/usr/share/dict/words");

		std::string line;

		while(getline(input, line)){

			std::transform(line.begin(), line.end(), line.begin(), ::toupper);

			dictwords.insert(line);
		}
	}
	std::set<std::string> dictwords;
};

int main(){

	struct dictstruct dict;

	//create a fake plate
	std::string plate = "RT 123SO";


	std::cout << "original plate:" << std::endl << plate << std::endl;

	plate.erase(remove_if(plate.begin(), plate.end(), [](char c) { 
				return !isalpha(c); 
				} ), plate.end());



	//start with full alphabet, gradually replace with shorter words until find minimum length
	std::string word1 = "abcdefghijklmnopqrustuvwxyz";

	std::set<std::string>::iterator it = dict.dictwords.begin();

	//attempt to find the shortest word in dictionary 

	while (it != dict.dictwords.end()) {
		int count = 0;
		for(char i : plate){
			if((*it).find(i) != std::string::npos){
				count+=1;
			}
		}
		if(count == plate.length() && count < word1.length()){
			word1 = (*it);
		}
		it++;
	}
	std::cout << "words from plate:" << std::endl;
	std::cout << word1 << std::endl;


}
