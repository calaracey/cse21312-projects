#include <iostream>
#include <set>
#include <unordered_set>

int main() {
	int numarr[] = {1,4,5,4,4,3,2,6,2,5};
	int target = 8;
	std::unordered_set<int> hashset;
	std::set<std::set<int>> sumpairs;
	for (int i = 0; i < 10; i++) {
		int complement = target - numarr[i];
		if (hashset.find(complement) != hashset.end()) {
			std::set<int> pair;
			pair.insert(numarr[i]);
			pair.insert(complement);
			sumpairs.insert(pair);
		}
		else {
			hashset.insert(numarr[i]);
		}
	}
	for (std::set<std::set<int>>::iterator outerit = sumpairs.begin(); outerit != sumpairs.end(); outerit ++ ) {
		std::cout << "Pair: ";
		if (outerit->size() == 1) {
			for (std::set<int>::iterator innerit = outerit->begin(); innerit != outerit->end(); innerit ++) {
				std::cout << (*innerit) << " " << (*innerit) << " ";
			}
		} else {
			for (std::set<int>::iterator innerit = outerit->begin(); innerit != outerit->end(); innerit ++) {
				std::cout << (*innerit) << " ";
			}
		}
		std::cout << std::endl;

	}
}
