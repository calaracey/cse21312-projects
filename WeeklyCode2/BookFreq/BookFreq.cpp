#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
int main(int argc, char** argv) {

	std::unordered_map<std::string,int> wordmap;

	std::ifstream inputfile;
	inputfile.open(argv[1]);
	
	std::string word;
	while (inputfile >> word) {
		if (wordmap.find(word) != wordmap.end()) {
			wordmap[word] = wordmap[word] += 1;
		}
		else { 
			wordmap[word] = 1;
		
		}
	}
	
	for (std::unordered_map<std::string, int>::iterator it = wordmap.begin(); it != wordmap.end(); it++) {
		std::cout << "Word: " << it->first << " Count: " << it->second << std::endl;
	}
}
	
	
		 
