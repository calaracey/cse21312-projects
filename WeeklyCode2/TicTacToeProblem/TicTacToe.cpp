/**********************************************
* File: TicTacToe.cpp
* Author: Connor Laracey
* Email: claracey@nd.edu
* TicTacToe.cpp determines the winner of a tic tac toe game 
* command to compile: g++ -std=c++11 TicTacToe.cpp -o TicTacToe
**********************************************/
#include <unordered_map>
#include <string>
#include <stdio.h>
#include <iostream>
typedef enum {CROSS, CIRCLE, EMPTY} Square;
/********************************************
* Function Name  : getSquareStatus 
* Pre-conditions : board, row, column 
* Post-conditions: Square 
* Returns the current status of a particular square 
********************************************/

Square getSquareStatus(Square board[][3], int row, int column) {
	return board[row][column];
}
/********************************************
* Function Name  : checkRows 
* Pre-conditions : board 
* Post-conditions: Square 
* Checks rows for possible victory 
********************************************/

Square checkRows(Square board[][3]) {
	int length = sizeof(board[0])/sizeof(board[0][0]);
	for (int i = 0; i < length; i++) {
		Square first = getSquareStatus(board,i,1);
		bool win = true;
		if (first == EMPTY) {
			continue;
		}
		for (int j = 0; j < length; j++) {
			if (getSquareStatus(board,i,j) != first) {
				win = false;
				break;
			}
		}
		if (win) {
		return first;
		}
	}
	return EMPTY;
}
/********************************************
* Function Name  : checkColumns 
* Pre-conditions : board 
* Post-conditions: Square 
* Checks columns for possible victory 
********************************************/

Square checkColumns(Square board[][3]) {
	int length = sizeof(board[0])/sizeof(board[0][0]);
	for (int i = 0; i < length; i++) {
		Square first = getSquareStatus(board,1,i);
		bool win = true;
		if (first == EMPTY) {
			continue;
		}
		for (int j = 0; j < length; j++) {
			if (getSquareStatus(board,j,i) != first) {
				win = false;
				break;
			}
		}
		if (win) {
		return first;
		}
	}
	return EMPTY;
}
/********************************************
* Function Name  : checkDiagonal 
* Pre-conditions : board 
* Post-conditions: Square 
* checks diagonals for possible victory 
********************************************/

Square checkDiagonal(Square board[][3]) {
	int length = sizeof(board[0])/sizeof(board[0][0]);
	Square first = getSquareStatus(board, 0, 0);
	if (first == EMPTY) {
		return EMPTY;
	}
	for (int i = 1; i < length; i++) {
		if (board[i][i] != first) {
			return EMPTY;
		}	
	}
	return first;
}
/********************************************
* Function Name  : checkReverseDiagonals 
* Pre-conditions : board 
* Post-conditions: Square 
* Checks diagonals for possible victory 
********************************************/

Square checkReverseDiagonal(Square board[][3]) {
	int length = sizeof(board[0])/sizeof(board[0][0]);
	Square first = getSquareStatus(board, 0, length-1);
	if (first == EMPTY) {
		return EMPTY;
	}
	for (int i = 1; i < length; i++) {
		if (board[i][length-1-i] != first) {
			return EMPTY;
		}	
	}
	return first;
}
/********************************************
* Function Name  : Winner 
* Pre-conditions : board 
* Post-conditions: Square 
* Returns the overall winner of the game,
* by calling individual row, column, diagonal
* checkers 
********************************************/

Square Winner(Square board[][3]) {
	if (checkRows(board) != EMPTY) {
		return checkRows(board);
	}
	if (checkColumns(board) != EMPTY) {
		return checkColumns(board);
	}
	if (checkDiagonal(board) != EMPTY) {
		return checkDiagonal(board);
	}
	if (checkReverseDiagonal(board) != EMPTY) {
		return checkReverseDiagonal(board);
	}
	return EMPTY;
/********************************************
* Function Name  : PrintResult 
* Pre-conditions : Square 
* Post-conditions: none 
* prints final outcome 
********************************************/
}
void PrintResult(Square result) {
	if (result == CROSS) {
		printf("Cross won the game.\n");
	}	
	else if(result == CIRCLE) {
		printf("Circle won the game.\n");
	}
	else {
		printf("The game ended in a draw.\n");
	}
	
}
int main() {
	Square board[3][3] = { {CIRCLE,EMPTY,EMPTY}, {CIRCLE,EMPTY,EMPTY}, {CIRCLE,EMPTY,EMPTY} };
	//PrintResult(Winner(board));
	/*
	Square board2[3][3] = { {CROSS,EMPTY,EMPTY}, {EMPTY,CROSS,EMPTY}, {EMPTY,EMPTY,CROSS} };
	PrintResult(Winner(board2));
	Square board3[3][3] = { {CROSS,CROSS,CROSS}, {EMPTY,EMPTY,EMPTY}, {EMPTY,EMPTY,EMPTY} };
	PrintResult(Winner(board3));
	Square board4[3][3] = { {EMPTY,EMPTY,CIRCLE}, {EMPTY,CIRCLE,EMPTY}, {CIRCLE,EMPTY,EMPTY} };
	PrintResult(Winner(board4));
	Square board5[3][3] = { {EMPTY,EMPTY,EMPTY}, {EMPTY,EMPTY,EMPTY}, {EMPTY,EMPTY,EMPTY} };
	PrintResult(Winner(board5));
	*/
	std::string boardstring;
	for (int i=0; i < 3; i++) {
		for (int j=0; j<3; j++) {
			if (board[i][j] == CROSS) {
				boardstring += "X";
			} else if (board[i][j] == CIRCLE) {
				boardstring += "O";
			} else {
				boardstring = " ";
			}
		}
	}
	std::unordered_map<std::string, Square> TTTmap;
	TTTmap[boardstring]=Winner(board);
}
