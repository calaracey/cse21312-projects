/**********************************************
 * File: ProjectDependency.cpp
 * Author: Connor Laracey
 * Email: claracey@nd.edu
 * Finds proper order to complete projects 
 **********************************************/
#include <vector>
#include <utility>
#include <algorithm>

/********************************************
 * Function Name  : DFSearch 
 * Pre-conditions : vector<vector<int>>,
 * vector<bool> x2, int, vector<int> 
 * Post-conditions: bool 
 * determines if the projects/dependencies are cyclical,
 * by performing DFS
 * which would result in no possible order
 * in which to complete projects (because you
 * would never satisfy the dependencies).
 * If it returns false, program returns empty
 * vector. If true, projects are pushed into
 * order vector in reverse order
 ********************************************/

bool DFSearch(std::vector<std::vector<int>>& g, std::vector<bool>& left, std::vector<bool>& handled, int proj, std::vector<int>& order) {
	if (left[proj]) {
		return false;
	}
	if (handled[proj]) {
		return true;
	}
	left[proj] = handled[proj] = true;
	for (int neighbor : g[proj]) {
		if (!DFSearch(g, left, handled, neighbor, order)) {
			return false;
		}
	}
	order.push_back(proj);
	left[proj] = false;
	return true;
}
/********************************************
 * Function Name  : FindOrder 
 * Pre-conditions : int, vector<pair<int,int>> 
 * Post-conditions: vector<int> 
 * initializes vectors, calls DFS
 * returns final order 
 ********************************************/
std::vector<int> FindOrder(int nProjects, std::vector<std::pair<int, int>>& dependencies) {
	std::vector<std::vector<int>> g(nProjects);
	for (auto projpair : dependencies) {
		g[projpair.second].push_back(projpair.first);
	}
	std::vector<int> order;
	std::vector<bool> left(nProjects, false), handled(nProjects, false);
	for (int i = 0; i < nProjects; i++) {
		if (!handled[i] && !DFSearch(g, left, handled, i, order)) {
			return {};
		}
	}
	//order vector is inserted in reverse in DFSearch function, so we must reverse for human readable format
	reverse(order.begin(), order.end());
	return order;
}
//main function just creates a test case and prints results:
int main() {
	int nProjects = 4;
	printf("Total number of projects: %d\n",nProjects);
	printf("Proj : Dependency\n");
	std::vector<std::pair<int, int>> dependencies;
	std::pair <int,int> project1 (1,0);
	std::pair <int,int> project2 (2,1);	
	std::pair <int,int> project3 (1,3);
	dependencies.push_back(project1);
	dependencies.push_back(project2);
	dependencies.push_back(project3);
	for (std::vector<std::pair<int, int>>::iterator it = dependencies.begin(); it != dependencies.end(); it++) {
		printf("%d : %d\n",(*it).first,(*it).second);
	}
	std::vector<int> results = FindOrder(nProjects, dependencies);
	printf("final order:\n");
	if (results.size() == 0) {
		printf("no possible order\n");
		return -1;
	}
	for (std::vector<int>::iterator it = results.begin(); it != results.end(); it++) {
		printf("%d\n", (*it));
	}
}

