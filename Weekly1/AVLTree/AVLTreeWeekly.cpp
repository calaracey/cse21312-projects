/**********************************************
* File: AVLTreeWeekly.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the driver function for the solution to 
* the Weekly Coding Assignment for the AVL Tree
*
* Compilation Instructions:
* g++ -g -std=c++11 -Wpedantic AVLTreeWeekly.cpp -o AVLTreeWeekly 
* ./AVLTreeWeekly
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

struct URL{
	
	std::string url;
	std::string webName; 
	
	/********************************************
	* Function Name  : URL
	* Pre-conditions : LastName
	* Post-conditions: Author(std::string url, std::string webName)
	* 
	* Constructor for the URL 
	********************************************/
	URL(std::string url, std::string webName) : url(url), webName(webName) {}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const URL& rhs
	* Post-conditions: bool
	* 
	* Overloaded < operator 
	********************************************/
	bool operator<(const URL& rhs) const{
		
		return url < rhs.url;
	}
	
	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Author& rhs
	* Post-conditions: bool
	* 
	* Overloaded == operator
	********************************************/
	bool operator==(const URL& rhs) const{
		
		return url == rhs.url;
	}
	
	/********************************************
	* Function Name  : operator<<
	* Pre-conditions : std::ostream& outStream, const URN& printURL
	* Post-conditions: friend std::ostream&
	* 
	* Overloaded friend function for URL 
	********************************************/
	friend std::ostream& operator<<(std::ostream& outStream, const URL& printURL);
};

std::ostream& operator<<(std::ostream& outStream, const URL& printURL){
	
	outStream << printURL.url << ", " << printURL.webName;
	
	return outStream;
}

/********************************************
* Function Name  : insertURLs
* Pre-conditions : AVLTree<URL>* searchEngine
* Post-conditions: none
* 
* Function has 10 hard-coded URLs and inserts them
* out of order into the AVL Tree 
********************************************/
void insertURLs(AVLTree<URL>* searchEngine){
	
	searchEngine->insert(URL("cse.nd.edu", "Notre Dame CSE"));
	
	searchEngine->insert(URL("google.com", "Google"));
	
	searchEngine->insert(URL("indeed.com/q-Computer-Science-Intern-jobs.html", "CS Internships"));
	
	searchEngine->insert(URL("und.com", "Notre Dame Athletics"));
	
	searchEngine->insert(URL("twitter.com", "Twitter"));
	
	searchEngine->insert(URL("sites.nd.edu/mmorrison", "Dr. Morrison's page"));
	
	searchEngine->insert(URL("gayle.com", "Cracking the Coding Interview"));
	
	searchEngine->insert(URL("facebook.com", "Facebook"));
	
	searchEngine->insert(URL("espn.com", "ESPN"));
	
	searchEngine->insert(URL("ieee.org", "IEEE Official Website"));
	
	std::cout << "The tree after the initial insertions is: " << std::endl;
	
	searchEngine->printTree( );
	
}

void deleteRoot(AVLTree<URL>* searchEngine){
	
	std::cout << std::endl << "Finding and deleting the root" << std::endl;
	
	// New function getRoot was written in AVLTree.
	URL theRoot = searchEngine->getRoot();
	
	std::cout << "The root of the AVL Tree is: " << theRoot << std::endl;
	
	searchEngine->remove(theRoot);
	
	// Check, does it contain the old root?
	if(searchEngine->contains(theRoot)){
		std::cout << "Deletion failed" << std::endl;
		exit(-1);
	}
	
	std::cout << std::endl << "The tree after deleting the root node is: " << std::endl;
	
	searchEngine->printTree( );
	
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    
	// Create an AVL Tree for the searchEngine
	AVLTree<URL> searchEngine;
	
	// Insert the 10 URLs
	insertURLs(&searchEngine);
	
	// Delete the root 
	deleteRoot(&searchEngine);
	
    return 0;
}
